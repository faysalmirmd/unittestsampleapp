﻿using Employee.App.Repository;
using Employee.App.Services;
using Employee.App.Models;
using NUnit.Framework;
using System;
using Moq;
using Employee.App.Exception;
using System.Collections.Generic;

namespace Test.EmployeeApp
{
    [TestFixture]
    public class EmployeeUnitTest
    {
        private IEmployeeService _employeeServicService;
        private Mock<ITaxCalculatorService> _taxService;
        private Mock<IPTOCalculatorService> _ptoService;
        private Mock<IEmployeeRepository> _employeeRepo;

        [SetUp]
        public void Setup()
        {
            _taxService = new Mock<ITaxCalculatorService>(MockBehavior.Strict);
            _ptoService = new Mock<IPTOCalculatorService>(MockBehavior.Strict);
            _employeeRepo = new Mock<IEmployeeRepository>(MockBehavior.Strict);
            _employeeServicService = new EmployeeService(_ptoService.Object, _taxService.Object, _employeeRepo.Object);
            _taxService.Setup(ts => ts.GetTax(It.IsAny<double>())).Returns<double>(amt => amt * 0.15);
        }

        [Test]
        public void GetTotalTax_Employee_ShouldReturnTotaltax()
        {
            var employee = new Employee.App.Models.Employee()
            {
                FirstName = "Team",
                LastName = "Southe",
                Level = 1,
                JoiningDate = DateTime.Parse("2017/01/01 10:00"),
                AllowedPTOs = 20,
                PTO_Taken = 2,
                EmployeeId = 100
            };

            _taxService.Setup(ts => ts.GetTax(It.IsAny<double>())).Returns<double>(amt => amt*0.25);
            var actual = _employeeServicService.GetTotalTax(employee);
            var expected = Math.Floor((DateTime.Now - employee.JoiningDate).TotalDays / 30) * employee.Salary * 0.25;
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void PromoteEmployee_Employee_ShouldPromote()
        {
            _ptoService.Setup(pto => pto.GetEarnedNumberOfPtos(It.IsAny<Employee.App.Models.Employee>())).Returns(5);
            var employeeBeforePromotion = GetSampleEmployeeToBePrmoted();

            var promotedEmployee = _employeeServicService.PromoteEmployee(GetSampleEmployeeToBePrmoted());
            
            Assert.IsInstanceOf<Employee.App.Models.Employee>(promotedEmployee);
            Assert.IsNotNull(promotedEmployee);
            Assert.AreEqual(employeeBeforePromotion.Level + 1, promotedEmployee.Level);
            Assert.AreEqual(employeeBeforePromotion.Salary + 1000, promotedEmployee.Salary);
            Assert.AreEqual(employeeBeforePromotion.AllowedPTOs + 2, promotedEmployee.AllowedPTOs);
        }

        [Test]
        public void PromoteEmployee_Employee_ShouldThrowNotEligibleForPromotionException()
        {
            _ptoService.Setup(pto => pto.GetEarnedNumberOfPtos(It.IsAny<Employee.App.Models.Employee>())).Returns(2);
            var employeeBeforePromotion = GetSampleEmployeeToBePrmoted();
            employeeBeforePromotion.PTO_Taken = 5;
            Assert.Throws<NotEligibleForPromotionException>(() =>_employeeServicService.PromoteEmployee(employeeBeforePromotion));
        }

        [Test]
        public void EmployeesToBePromoted_ShouldreturnListOfEmployeestoBePromoted()
        {
            _ptoService.Setup(pto => pto.GetEarnedNumberOfPtos(It.IsAny<Employee.App.Models.Employee>())).Returns(5);
            _employeeRepo.Setup(er => er.GetAll()).Returns(GetListOfEmployees());
            var employees = _employeeServicService.EmployeesToBePromoted();
            var employee1 = employees.Find(e => e.EmployeeId == 4);
            var employee2 = employees.Find(e => e.EmployeeId == 5);

            Assert.IsNotEmpty(employees);
            Assert.AreEqual(2, employees.Count);
            Assert.IsNotNull(employee1);
            Assert.IsNotNull(employee2);
        }

        private static Employee.App.Models.Employee GetSampleEmployeeToBePrmoted()
        {
            return new Employee.App.Models.Employee()
            {
                FirstName = "Team",
                LastName = "Southe",
                Level = 1,
                JoiningDate = DateTime.Parse("2017/01/01 10:00"),
                AllowedPTOs = 20,
                PTO_Taken = 2,
                EmployeeId = 100,
                Performance_Rating = 80
            };
        }

        private static List<Employee.App.Models.Employee> GetListOfEmployees()
        {
            return new List<Employee.App.Models.Employee>
            {
                new Employee.App.Models.Employee {FirstName = "John", LastName = "Doe", AllowedPTOs = 20, EmployeeId = 1, JoiningDate = DateTime.Parse("2017/01/01 10:00"), Level = 1, PTO_Taken = 6, Salary = 1000, Performance_Rating = 75 },
                new Employee.App.Models.Employee {FirstName = "Jane", LastName = "Doe", AllowedPTOs = 22, EmployeeId = 2, JoiningDate = DateTime.Parse("2017/01/01 10:00"), Level = 2, PTO_Taken = 3, Salary = 2000, Performance_Rating = 55 },
                new Employee.App.Models.Employee {FirstName = "John", LastName = "Smith", AllowedPTOs = 24, EmployeeId = 3, JoiningDate = DateTime.Parse("2017/01/01 10:00"), Level = 3, PTO_Taken = 10, Salary = 3000, Performance_Rating = 45 },
                new Employee.App.Models.Employee {FirstName = "Matthew", LastName = "MacDonald", AllowedPTOs = 24, EmployeeId = 4, JoiningDate = DateTime.Parse("2017/01/01 10:00"), Level = 3, PTO_Taken = 4, Salary = 3000, Performance_Rating = 100 },
                new Employee.App.Models.Employee {FirstName = "Andrew", LastName = "MacDonald", AllowedPTOs = 26, EmployeeId = 5, JoiningDate = DateTime.Parse("2017/01/01 10:00"), Level = 4, PTO_Taken = 4, Salary = 4000, Performance_Rating = 75 }
            };
        }
    }
}
