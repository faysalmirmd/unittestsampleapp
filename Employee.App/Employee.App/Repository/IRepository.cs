﻿using Employee.App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee.App.Repository
{
    public interface IRepository<out T> where T : DomainObject
    {
        IEnumerable<T> GetAll();
    }

}
