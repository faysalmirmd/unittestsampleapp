﻿using Employee.App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee.App.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private static IEnumerable<Models.Employee> _people;

        private static IEnumerable<Models.Employee> GetPeople()
        {
            if (_people != null) return _people;

            _people = new List<Models.Employee>
        {
            new Models.Employee {FirstName = "John", LastName = "Doe", AllowedPTOs = 20, EmployeeId = 1, JoiningDate = DateTime.Parse("2017/01/01 10:00"), Level = 1, PTO_Taken = 2, Salary = 1000, Performance_Rating = 75 },
            new Models.Employee {FirstName = "Jane", LastName = "Doe", AllowedPTOs = 22, EmployeeId = 2, JoiningDate = DateTime.Parse("2017/01/01 10:00"), Level = 2, PTO_Taken = 3, Salary = 2000, Performance_Rating = 55 },
            new Models.Employee {FirstName = "John", LastName = "Smith", AllowedPTOs = 24, EmployeeId = 3, JoiningDate = DateTime.Parse("2017/01/01 10:00"), Level = 3, PTO_Taken = 10, Salary = 3000, Performance_Rating = 85 },
            new Models.Employee {FirstName = "Matthew", LastName = "MacDonald", AllowedPTOs = 24, EmployeeId = 4, JoiningDate = DateTime.Parse("2017/01/01 10:00"), Level = 3, PTO_Taken = 4, Salary = 3000, Performance_Rating = 100 },
            new Models.Employee {FirstName = "Andrew", LastName = "MacDonald", AllowedPTOs = 26, EmployeeId = 5, JoiningDate = DateTime.Parse("2017/01/01 10:00"), Level = 4, PTO_Taken = 4, Salary = 4000, Performance_Rating = 75 }
        };

            return _people;
        }

        public IEnumerable<Models.Employee> GetAll()
        {
            return GetPeople();
        }
    }
}
