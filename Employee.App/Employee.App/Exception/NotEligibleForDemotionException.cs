﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee.App.Exception
{
    public class NotEligibleForDemotionException : System.Exception
    {
        public NotEligibleForDemotionException(string message): base(message)
        {

        }
    }
}
