﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee.App.Models
{
    public abstract class DomainObject
    { }

    public class Person : DomainObject
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class Employee: Person
    {
        public string Name { get { return FirstName + " " + LastName; } }

        public int EmployeeId { get; set; }

        public DateTime JoiningDate { get; set; }

        public double Salary { get; set; }

        public int PTO_Taken { get; set; }

        public int Level { get; set; }

        public int AllowedPTOs { get; set; }

        public int Performance_Rating { get; set; }
    }
}
