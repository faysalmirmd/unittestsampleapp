﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Employee.App.Models;

namespace Employee.App.Services
{
    public interface IPTOCalculatorService
    {
        int GetEarnedNumberOfPtos(Models.Employee employee);
    }
}
