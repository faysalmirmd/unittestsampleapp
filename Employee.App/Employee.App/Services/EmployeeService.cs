﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Employee.App.Models;
using Employee.App.Repository;
using Employee.App.Exception;

namespace Employee.App.Services
{
    public class EmployeeService : IEmployeeService
    {
        private IPTOCalculatorService _pTOCalculatorService;
        private ITaxCalculatorService _taxCalculatorService;
        private IEmployeeRepository _employeeRepository;

        public EmployeeService(IPTOCalculatorService pTOCalculatorService, ITaxCalculatorService taxCalculatorService, IEmployeeRepository employeeRepository)
        {
            _pTOCalculatorService = pTOCalculatorService;
            _taxCalculatorService = taxCalculatorService;
            _employeeRepository = employeeRepository;
        }

        public Models.Employee DemoteEmployee(Models.Employee employee)
        {
            if (IsEmployeeEligibleForDemotion(employee))
            {
                employee.AllowedPTOs -= 2;
                employee.Salary -= 1000;
                employee.Level--;
            }
            else
            {
                throw new NotEligibleForDemotionException("Not Elligible for demotion");
            }
            return employee;
        }

        public List<Models.Employee> EmployeesToBeDemoted()
        {
            return _employeeRepository.GetAll().Where(e => IsEmployeeEligibleForDemotion(e)).ToList();
        }

        public List<Models.Employee> EmployeesToBePromoted()
        {
            return _employeeRepository.GetAll().Where(e => IsEmployeeEligibleForPromotion(e)).ToList();
        }

        public double GetTotalTax(Models.Employee employee)
        {
            return _taxCalculatorService.GetTax(Math.Floor((DateTime.Now - employee.JoiningDate).TotalDays / 30) * employee.Salary);
        }

        public bool IsEmployeeEligibleForDemotion(Models.Employee employee)
        {
            return employee.Performance_Rating < 70 || _pTOCalculatorService.GetEarnedNumberOfPtos(employee) < employee.PTO_Taken;
        }

        public bool IsEmployeeEligibleForPromotion(Models.Employee employee)
        {
            return employee.Performance_Rating >= 70 && _pTOCalculatorService.GetEarnedNumberOfPtos(employee) >= employee.PTO_Taken;
        }

        public Models.Employee PromoteEmployee(Models.Employee employee)
        {
            if (IsEmployeeEligibleForPromotion(employee))
            {
                employee.AllowedPTOs += 2;
                employee.Salary += 1000;
                employee.Level++;
            }
            else {
                throw new NotEligibleForPromotionException("Not Elligible for promotion");
            }
            return employee;
        }
    }
}
