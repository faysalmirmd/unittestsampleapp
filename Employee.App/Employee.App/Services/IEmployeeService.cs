﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employee.App.Services
{
    public interface IEmployeeService
    {
        Models.Employee PromoteEmployee(Models.Employee employee);
        Models.Employee DemoteEmployee(Models.Employee employee);
        double GetTotalTax(Models.Employee employee);
        List<Models.Employee> EmployeesToBePromoted();
        List<Models.Employee> EmployeesToBeDemoted();
    }
}
