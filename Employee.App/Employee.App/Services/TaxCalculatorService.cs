﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Employee.App.Models;

namespace Employee.App.Services
{
    public class TaxCalculatorService : ITaxCalculatorService
    {
        const double TAX_RATE = 0.15;

        public double GetTax(double amount)
        {
            return amount * TAX_RATE;
        }
    }
}
