﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Employee.App.Models;

namespace Employee.App.Services
{
    public class PTOCalculatorService : IPTOCalculatorService
    {
        public int GetEarnedNumberOfPtos(Models.Employee employee)
        {
            return (int)Math.Floor(((DateTime.Now - employee.JoiningDate).Days * employee.AllowedPTOs / 360.0));
        }
    }
}
